# ATTiny EM-based Security Laser system

This is a tiny project to achieve two goals:

1) Get my DigiSpark ATTiny85 dev chain up and working
2) Use a laser as a security thing
3) Be awesome

It achieved all of these goals. I was able to use an ATTiny85 on the DigiSpark USB board to read a EM-based presence detection sensor for when people are moving nearby, then light up a really simple 5v sub-5mW laser.

That's awesome, so #3 is justified.

License:
    Do what you will, public domain
    Aaron S. Crandall <acrandal@gmail.com>, 2018
