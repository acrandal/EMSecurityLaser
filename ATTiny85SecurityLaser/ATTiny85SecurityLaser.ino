// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(1, OUTPUT); //LED on Model A   
  pinMode(0, INPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  if( digitalRead(0) == HIGH ) {
    digitalWrite(1, HIGH);
  }
  else
  {
    digitalWrite(1, LOW); 
  }
}
